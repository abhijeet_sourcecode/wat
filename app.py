from flask import Flask
import config
from es.views import es_view


def create_app():
    # static files
    app = Flask(__name__, static_url_path='/static')

    # views
    app.register_blueprint(es_view)

    # apis

    return app
