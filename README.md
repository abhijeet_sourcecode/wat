# WAT Manual
A search engine to search for quotes from Shakespeare's plays.


## to install required libraries
virtualenv server_env
source server_env/bin/activate
pip install -r requirements.txt 


## to compile scss
sass --watch scss/style.scss:css/style.css

## to run
python runner.py

## site address
http://127.0.0.1:9100/
