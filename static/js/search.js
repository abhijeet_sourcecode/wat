function disableSearch(){
	$("#search-text").prop('disabled', true);
}

function enableSearch(){
	$("#search-text").prop('disabled', false);
}

$("#search-form").submit(function(event) {
		event.preventDefault();
		var search_disabled = $("#search-text").prop("disabled");
		if (search_disabled){
			console.log('no submit');
			return;
		}
		disableSearch();
		var search_text = $("#search-text").val();
		$.pjax({
				type: "GET",
				url: "/",
				data: {"search_term": search_text}, 
				container: "#results",
				timeout: 2000
		}).done(enableSearch());
});

$("#search-icon").click(function(event) {
		event.preventDefault();
		$("#search-form").submit();
});
