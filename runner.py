from app import create_app


def healthcheck(app):
    @app.route("/healthcheck")
    def _success():
        return ('', 200)


if __name__ == '__main__':
    app = create_app()
    healthcheck(app)
    app.run(debug=True, port=9100)
