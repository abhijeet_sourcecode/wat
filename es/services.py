from elasticsearch import Elasticsearch
import locale


locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
INDEX_NAME = 'shakespeare'
DEFAULT_SEARCH_TERM = "where art thou"

es = Elasticsearch()


def get_search_results(search_term):
    if not search_term or not search_term.strip():
        search_term = DEFAULT_SEARCH_TERM

    search_body = {
        "query": {
            "match": {
                "text_entry": search_term
            }
        }
    }
    hit_count = 0
    search_result = []
    res = es.search(index=INDEX_NAME, body=search_body)
    hit_count = res['hits']['total']
    if hit_count and hit_count > 0:
        hit_count = locale.format("%d", hit_count, grouping=True)
        search_result = [result['_source'] for result in res['hits']['hits'] if result['_source']['speaker'].strip()]

    return search_term, hit_count, search_result
