from flask import request
from flask import Blueprint
from flask import render_template
from .services import get_search_results

es_view = Blueprint('es_view', __name__)


@es_view.route('/')
def search():
    template_name = 'index.html'
    search_term = request.args.get('search_term', None)
    is_pjax_request = 'X-Pjax' in request.headers
    if is_pjax_request:
        template_name = 'index-pjax.html'
    search_term, hit_count, search_result = get_search_results(search_term)
    return render_template(template_name, search_term=search_term, hit_count=hit_count, search_result=search_result)
